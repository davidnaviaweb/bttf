<?php
/*
Plugin Name: Back to the Future Almanac Plugin
Description: Plugin para gestionar datos de futuros resultados deportivos, y así hacerse rico sin esfuerzo.
Version: 1.0
Text-domain: bttf-almanac
*/

if(class_exists('BTTF_Almanac')){
    new BTTF_Almanac();
}

class BTTF_Almanac
{

    private $plugin_slug;
    private $post_type;
    private $post_type_tax;
    private $unknown;


    public function __construct()
    {

        $this->plugin_slug = 'bttf-almanac';
        $this->post_type = 'bttf_matches';
        $this->post_type_tax = 'bttf_matches_areas';
        $this->unknown = plugins_url( '/img/unknown.jpg', __FILE__ );

        register_activation_hook( __FILE__, array( &$this, 'activate' ) );
        register_deactivation_hook( __FILE__, array( &$this, 'deactivate' ) );

        // Soporte de idioma - Text-domain: bttf-almanac
        add_action('init', array(&$this, 'load_text_domain'));

        // Taxonomías
        add_action('init', array(&$this, 'register_taxonomy'));

        // CPT
        add_action('init', array(&$this, 'register_cpt'));

        // Metaboxes
        add_action('add_meta_boxes', array(&$this, 'add_metaboxes'));

        // Publish / Update
        add_action('save_post_'.$this->post_type, array(&$this, 'bttf_almanac_results_save_match'));
        add_action('publish_'.$this->post_type, array(&$this, 'bttf_almanac_results_save_match'));

        // Shortcode
        add_shortcode('bttf_almanac_results', array(&$this, 'bttf_almanac_results_render_shortcode'));
        add_action('init', array(&$this, 'bttf_almanac_results_shortcode_button_init'));
    }

    public function activate()
    {
        global $wp_rewrite;
        $wp_rewrite->flush_rules();
    }

    public function deactivate()
    {
    }

    public function load_text_domain()
    {
        load_plugin_textdomain($this->plugin_slug, false, dirname(plugin_basename(__FILE__)) . '/languages/');
    }

    public function register_taxonomy()
    {
        $capabilities = array(
            'manage_terms' => 'manage_options',
            'edit_terms' => 'manage_options',
            'delete_terms' => 'manage_options',
            'assign_terms' => 'manage_options'
        );

        $labels = array(
            'name' => __('Areas', $this->plugin_slug),
            'singular_name' => __('Area', $this->plugin_slug),
            'search_items' => __('Search Areas', $this->plugin_slug),
            'all_items' => __('All Areas', $this->plugin_slug),
            'parent_item' => __('Parent Area', $this->plugin_slug),
            'edit_item' => __('Edit Area', $this->plugin_slug),
            'update_item' => __('Update Area', $this->plugin_slug),
            'add_new_item' => __('New Area', $this->plugin_slug),
            'new_item_name' => __('New Area name', $this->plugin_slug),
            'menu_name' => __('Areas', $this->plugin_slug),
        );


        register_taxonomy($this->post_type_tax, $this->plugin_slug, array(
            'hierarchical' => true,
            'labels' => $labels,
            'show_ui' => true,
            'capabilities' => $capabilities
        ));
    }

    public function register_cpt()
    {
         $capabilities = array(
            'publish_posts' => 'manage_options',
            'edit_posts' => 'manage_options',
            'edit_others_posts' => 'manage_options',
            'delete_posts' => 'manage_options',
            'delete_others_posts' => 'manage_options',
            'read_private_posts' => 'manage_options',
            'edit_post' => 'manage_options',
            'delete_post' => 'manage_options',
            'read_post' => 'manage_options',
        );

        $labels = array(
            'name' => __('Matches', $this->plugin_slug),
            'singular_name' => __('Match', $this->plugin_slug),
            'add_new' => __('Add new', $this->plugin_slug),
            'add_new_item' => __('Add new match', $this->plugin_slug),
            'edit' => __('Edit', $this->plugin_slug),
            'edit_item' => __('Edit match', $this->plugin_slug),
            'new_item' => __('New match', $this->plugin_slug),
            'view' => __('View', $this->plugin_slug),
            'view_item' => __('View match', $this->plugin_slug),
            'search_items' => __('Search matches', $this->plugin_slug),
            'not_found' => __('Matches not found', $this->plugin_slug),
            'not_found_in_trash' => __('Matches not found in trash', $this->plugin_slug)
        );

        register_post_type($this->post_type,
            array(
                'labels' => $labels,
                'description' => __('Matches', $this->plugin_slug),
                'menu_icon' => 'dashicons-megaphone',
                'public' => true,
                'show_ui' => true,
                'hierarchical' => false,
                'supports' => array('title'),
                'capabilities' => $capabilities,
                'query_var' => true,
                'has_archive' => true,
                'taxonomies' => array('bttf_matches_areas'),
                'rewrite' => array('slug' => _x('matches', 'Slug for Match CPT', $this->plugin_slug), 'with_front' =>
                    false)
            )
        );

    }

    public function add_metaboxes()
    {

        $this->bttf_almanac_enqueue_assets();

        add_meta_box("bttf-match-data", __('Match data', $this->plugin_slug), array(&$this, 'bttf_match_data_metabox'), $this->post_type, 'normal');

        add_meta_box("bttf-match-file", __('Almanac page', $this->plugin_slug), array(&$this, 'bttf_match_file_metabox'), $this->post_type, 'normal');

        add_meta_box("bttf-match-video-link", __('Youtube video URL', $this->plugin_slug), array(&$this, 'bttf_match_video_link_metabox'), $this->post_type, 'normal');

    }


    /* MATCHES */

    public function bttf_match_data_metabox($post)
        {
            $bttf_match_local_team = get_post_meta($post->ID, 'bttf_match_local_team', true);
            $bttf_match_local_avatar = get_post_meta($post->ID, 'bttf_match_local_avatar', true);
            $bttf_match_local_score = get_post_meta($post->ID, 'bttf_match_local_score', true);
            $bttf_match_visitor_team = get_post_meta($post->ID, 'bttf_match_visitor_team', true);
            $bttf_match_visitor_avatar = get_post_meta($post->ID, 'bttf_match_visitor_avatar', true);
            $bttf_match_visitor_score = get_post_meta($post->ID, 'bttf_match_visitor_score', true);

            include('views/bttf_match_data_metabox.php');
        }

    public function bttf_match_file_metabox($post)
        {
            $bttf_match_file = get_post_meta($post->ID, 'bttf_match_file', true);

            include('views/bttf_match_file_metabox.php');
        }

    public function bttf_match_video_link_metabox($post)
        {
            $bttf_match_video_link = get_post_meta($post->ID, 'bttf_match_video_link', true);

            include('views/bttf_match_youtube_url_metabox.php');
        }

    public function bttf_almanac_results_save_match($post_id){


        if (isset($_POST['local_team'])) {
            update_post_meta($post_id, 'bttf_match_local_team', ($_POST['local_team']));
        }

        if (isset($_POST['local_avatar'])) {
            update_post_meta($post_id, 'bttf_match_local_avatar', ($_POST['local_avatar']));
        }

        if (isset($_POST['local_score'])) {
            update_post_meta($post_id, 'bttf_match_local_score', ($_POST['local_score']));
        }

         if (isset($_POST['visitor_team'])) {
            update_post_meta($post_id, 'bttf_match_visitor_team', ($_POST['visitor_team']));
        }

        if (isset($_POST['visitor_avatar'])) {
            update_post_meta($post_id, 'bttf_match_visitor_avatar', ($_POST['visitor_avatar']));
        }

        if (isset($_POST['visitor_score'])) {
            update_post_meta($post_id, 'bttf_match_visitor_score', ($_POST['visitor_score']));
        }

        if (isset($_POST['match_file'])) {
            update_post_meta($post_id, 'bttf_match_file', $_POST['match_file']);
        }

        if (isset($_POST['match_video_link'])) {
            update_post_meta($post_id, 'bttf_match_video_link', sanitize_text_field($_POST['match_video_link']));
        }
    }

    public function bttf_almanac_enqueue_assets(){

        // CSS
        wp_enqueue_style('thickbox');
        wp_enqueue_style('bttf-styles', plugins_url('/css/bttf-almanac.css', __FILE__));

        // Javascript
        wp_register_script($this->plugin_slug, plugins_url( '/js/bttf-scripts.js', __FILE__ ), array('jquery',
            'jquery-ui-core', 'jquery-ui-dialog', 'media-upload', 'thickbox'));

        $translation_array = array(
            'closeModal' => __( 'Close', $this->plugin_slug ),
            'acceptModal' => __( 'Accept', $this->plugin_slug ),
            'uploadImg' => __( 'Upload image', $this->plugin_slug ),
            'uploadPdf' => __( 'Upload PDF', $this->plugin_slug ),
            'fileurl_error' => __( 'Please watch out the instructions for uploading this PDF', $this->plugin_slug ),
            'writeCategory' => __( 'Write the slug of a category', $this->plugin_slug ),
            'emptyString' => __( 'You wrote nothing', $this->plugin_slug ),
            'shortcodeButtonTitle' => __( 'Add BTTF Almanac results', $this->plugin_slug ),
            'image_error' => __( 'This is not an allowed image file. Try it again.', $this->plugin_slug ),
            'pdf_error' => __( 'This is not a PDF file. Try it again.', $this->plugin_slug ),
            'change_pdf' => __( 'Change file', $this->plugin_slug ),
            'url_error' => __('This URL seems to be wrong. Check it out.', $this->plugin_slug)
        );

        wp_localize_script( $this->plugin_slug, 'bttf', $translation_array );
        wp_enqueue_script( $this->plugin_slug );
    }


    // Shortcode

    function bttf_almanac_results_render_shortcode($atts)
    {
        extract(shortcode_atts(array(
            'bttf_area' => '-1'
        ), $atts));

        if(term_exists( get_term($bttf_area)->name, $this->post_type_tax)){

            $args = array(
                'post_per_page' => -1,
                'post_type' => $this->post_type,
                'post_status' => 'publish',
                'order' => 'DESC',
                'orderby' => 'date',
                'tax_query' => array(
                    array(
                        'taxonomy' => $this->post_type_tax,
                        'field' => 'term_id',
                        'terms' => $bttf_area,
                    )
                )
            );

            $matches = new WP_Query( $args );

            if ( $matches->have_posts() ) {
                include('views/bttf_match_shortcode_table.php');
            } else {
                // no posts found
            }
        }
    }

    function bttf_almanac_results_shortcode_modal_box(){
        include('views/bttf_match_areas_modal_box.php');
    }

    function bttf_almanac_results_register_tinymce_plugin($plugin_array)
    {
        $plugin_array['bttf_almanac_results_button'] = plugins_url( 'js/bttf-shortcode.js', __FILE__ );
        return $plugin_array;
    }

    function bttf_almanac_results_add_tinymce_button($buttons)
    {
        add_meta_box("bttf-shortcode-modal-box", 'test', array(&$this, 'bttf_almanac_results_shortcode_modal_box'));

        //Add the button ID to the $button array
        $buttons[] = "bttf_almanac_results_button";
        return $buttons;
    }

    function bttf_almanac_results_shortcode_button_init()
    {

        //Abort early if the user will never see TinyMCE
        if (!current_user_can('edit_posts') && !current_user_can('edit_pages') && get_user_option('rich_editing') == 'true')
            return;

        wp_enqueue_style($this->plugin_slug, plugins_url('/css/shortcode-button.css', __FILE__));
        wp_enqueue_style ('wp-jquery-ui-dialog');

        //Add a callback to regiser our tinymce plugin
        add_filter('mce_external_plugins', array(&$this, 'bttf_almanac_results_register_tinymce_plugin'));

        // Add a callback to add our button to the TinyMCE toolbar
        add_filter('mce_buttons', array(&$this, 'bttf_almanac_results_add_tinymce_button'));
    }

}

