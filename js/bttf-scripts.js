jQuery(document).ready(function(){

    // Limit to one single sport area
    jQuery("#bttf_matches_areaschecklist input, #bttf_matches_areaschecklist-pop input")
        .each(function () {
            this.type = 'radio';
        }
    );

    // Image uploader
    jQuery('#upload-image-button-1').click(function (e) {
        tb_show(bttf.uploadImg, 'media-upload.php?type=file&amp;TB_iframe=true', false);
        var backup = window.send_to_editor;
        window.send_to_editor = function (html) {
            var classes = jQuery(html).attr('class');
            var id = classes.replace(/(.*?)wp-image-/, '');
            var fileurl = jQuery(html).attr('src');
            if (fileurl.match("jpg$|jpeg$|png$|gif$")) {
                jQuery('#local_avatar').val(id);
                jQuery('#local_avatar_canvas').attr('src', fileurl);
                tb_remove();
                window.send_to_editor = backup;
            } else {
                alert(bttf.image_error);
            }
        };
    });
    jQuery('#upload-image-button-2').click(function (e) {
        tb_show(bttf.uploadImg, 'media-upload.php?type=file&amp;TB_iframe=true', false);
        var backup = window.send_to_editor;
        window.send_to_editor = function (html) {
            var classes = jQuery(html).attr('class');
            var id = classes.replace(/(.*?)wp-image-/, '');
            var fileurl = jQuery(html).attr('src');
            if (fileurl.match("jpg$|jpeg$|png$|gif$")) {
                jQuery('#visitor_avatar').val(id);
                jQuery('#visitor_avatar_canvas').attr('src', fileurl);
                tb_remove();
                window.send_to_editor = backup;
            } else {
                alert(bttf.image_error);
            }
        };
    });

    // PDF uploader
    jQuery('#upload-match-file-button').click(function(e)
        {
            tb_show(bttf.uploadPdf, 'media-upload.php?type=file&amp;TB_iframe=true');
            var backup = window.send_to_editor;
            window.send_to_editor = function (html) {
                fileurl = jQuery(html).attr('href');
                if(fileurl != undefined){
                    if (fileurl.match("pdf$")) {
                        jQuery('#match_file').val(fileurl);
                        tb_remove();
                        jQuery('#upload-match-file-button').attr('value', bttf.change_pdf)
                        window.send_to_editor = backup;
                    }else{
                        alert(bttf.pdf_error);
                        tb_remove();
                        window.setTimeout(function () {
                            tb_show('', 'media-upload.php?type=file&amp;TB_iframe=true');
                        }, 500);
                    }
                }else{
                    alert(bttf.fileurl_error);
                }
            }
        }
    );

    // Youtube link checker
    jQuery('#match_video_link').keyup(
        function(e)
        {
            var url = jQuery(this).val();
            var regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/;
            var match = url.match(regExp);
            if (!match || match[7].length != 11){
                alert(bttf.url_error);
                jQuery(this).focus();
                jQuery(this).parents('tr').addClass('form-invalid');
                jQuery('#publish').attr("disabled", "disabled");
            }else{
                jQuery(this).parents('tr').removeClass('form-invalid');
                jQuery('#publish').attr("disabled", false);
            }
        }
    );

});

