// Shortcode
tinymce.create('tinymce.plugins.bttf_almanac_results_plugin', {
    init: function (ed, url) {
        // Register command for when button is clicked
        ed.addCommand('bttf_almanac_results_insert_shortcode', function () {

            var modalbox = jQuery("#bttf_areas_modalbox");
            var buttons = {};
            buttons[bttf.closeModal] = function() {
                jQuery(this).dialog('close');
            };
            buttons[bttf.acceptModal] =  function(){
                var area = jQuery('#bttf_areas_modalbox select option:selected').val();
                if(area == '-1'){
                    jQuery('#bttf_areas_modalbox #errmsg').show(0).delay(5000).hide(200);
                }else{
                    var content = '[bttf_almanac_results bttf_area="'+ area + '"]';
                    tinymce.execCommand('mceInsertContent', false, content);
                    jQuery(this).dialog('close');
                }
            };
            modalbox.dialog({
                'dialogClass'   : 'wp-dialog',
                'modal'         : true,
                'autoOpen'      : false,
                'closeOnEscape' : false,
                'buttons'       : buttons
            });
            event.preventDefault();
            modalbox.dialog('open');

        });

        // Register buttons - trigger above command when clicked
        ed.addButton('bttf_almanac_results_button', {
            title: bttf.shortcodeButtonTitle,
            cmd: 'bttf_almanac_results_insert_shortcode',
            icon: 'dashicons dashicons-grid-view'
        });
    },
});

// Register our TinyMCE plugin
// first parameter is the button ID1
// second parameter must match the first parameter of the tinymce.create() function above
tinymce.PluginManager.add('bttf_almanac_results_button', tinymce.plugins.bttf_almanac_results_plugin);


