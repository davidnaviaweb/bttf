<?php
$args = array ('taxonomy' => array($this->post_type_tax),
                'show_option_none' => __('Select area', $this->plugin_slug),
                'orderby' => 'name',
                'hide_empty' => 0,
                'echo' => 1
            );

?>
<div id="bttf_areas_modalbox" title="<?php _e( 'Add BTTF Almanac results', 'bttf-almanac' ); ?>">
    <table>
        <tr class="form-field">
            <th scope="row"><?php _e( 'Choose the sport area:', 'bttf-almanac' ); ?>&nbsp;</th>
            <td><?php wp_dropdown_categories($args) ?></td>
        </tr>
        <tr>
            <td colspan="2">
                <div id="errmsg">
                    <p><?php _e('You must select a valid area', 'bttf-almanac' ); ?></p>
                </div>
            </td>
        </tr>
    </table>
</div>