<p><i><?php _e('Here you could add any Youtube URL that contains any video related to this match.', 'bttf-almanac');
        ?></i></p>
<table class="fullwidth">
    <tr class="form-field">
        <th scope="row"><label for="match_video_link"><?php _e( 'Youtube Video URL', 'bttf-almanac' ); ?></label></th>
        <td>
            <input class="fullwidth" type="text" name="match_video_link" id="match_video_link" value="<?php if (isset($bttf_match_video_link)) echo $bttf_match_video_link; ?>"/>
        </td>
    </tr>
    <tr>
        <td></td>
        <td>
            <span class="description"><?php _e('Supported URL types:','bttf-almanac'); ?></span>
            <span class="description">
                <ul class="list">
                    <li>http://www.youtube.com/watch?v=0zM3nApSvMg&feature=feedrec_grec_index</li>
                    <li>http://www.youtube.com/user/IngridMichaelsonVEVO#p/a/u/1/QdK8U-VIH_o</li>
                    <li>http://www.youtube.com/v/0zM3nApSvMg?fs=1&amp;hl=en_US&amp;rel=0</li>
                    <li>http://www.youtube.com/watch?v=0zM3nApSvMg#t=0m10s</li>
                    <li>http://www.youtube.com/embed/0zM3nApSvMg?rel=0</li>
                    <li>http://www.youtube.com/watch?v=0zM3nApSvMg</li>
                    <li>http://youtu.be/0zM3nApSvMg</li>
                </ul>
            </span>
        </td>
    </tr>
</table>
