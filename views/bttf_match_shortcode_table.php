<h1 style="text-align: center; font-size:1.5em" colspan="2"><?php printf(__('Results for %s', $this->plugin_slug), get_term($bttf_area)->name); ?></h1>
<?php
foreach ($matches->posts as $match):
?>
<table>
    <tbody>
        <tr>
            <th style="text-align: center;border:2px solid">
                <img class="avatar" src="<?php echo wp_get_attachment_image_src(get_post_meta($match->ID, 'bttf_match_local_avatar', true),'thumbnail')[0];
                ?>" />
                <br/>
                <?php echo get_post_meta($match->ID, 'bttf_match_local_team', true); ?>
            </th>
            <th style="text-align: center;border:2px solid">
                <img class="avatar" src="<?php echo wp_get_attachment_image_src(get_post_meta($match->ID, 'bttf_match_visitor_avatar', true),'thumbnail')[0];
                ?>" />
                <br/>
                <?php echo get_post_meta($match->ID, 'bttf_match_visitor_team', true); ?>
            </th>
        </tr>
        <tr>
            <td>
                <h2 style="text-align: center; padding: 0; margin: 10px auto;">
                    <?php echo get_post_meta($match->ID, 'bttf_match_local_score', true); ?>
                </h2>
            </td>
            <td>
                <h2 style="text-align: center; padding: 0; margin: 10px auto;">
                    <?php echo get_post_meta($match->ID, 'bttf_match_visitor_score', true); ?>
                </h2>
            </td>
        </tr>
        <?php if (get_post_meta($match->ID, 'bttf_match_video_link',true)):
        preg_match("#(?<=v=)[a-zA-Z0-9-]+(?=&)|(?<=v\/)[^&\n]+(?=\?)|(?<=v=)[^&\n]+|(?<=youtu.be/)[^&\n]+#", get_post_meta($match->ID, 'bttf_match_video_link',true),$match);
                ?>
        <tr style="text-align: center">
            <td colspan="2">
                <iframe style="display: block;margin: 0 auto;" width="560" height="315" src="https://www.youtube.com/embed/<?php echo $match[0]; ?>" frameborder="0" allowfullscreen></iframe>
            </td>
        </tr>
        <?php endif; ?>
        <?php if (get_post_meta($match->ID, 'bttf_match_file',true)): ?>
            <tr>
                <td style="text-align: center;" colspan="2">
                    <a class="button" href="<?php echo get_post_meta($match->ID, 'bttf_match_file',true); ?>">
                        <?php _e('Download BTTF Almanac Sheet', $this->plugin_slug); ?>
                    </a>
                </td>
            </tr>
        <?php endif; ?>
    </tbody>
</table>
<?php endforeach; ?>