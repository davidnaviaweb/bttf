<table>
    <tr class="form-field">
        <th scope="col"><?php _e( 'Local team', 'bttf-almanac' ); ?></th>
        <th scope="col"><?php _e( 'Visitor team', 'bttf-almanac' ); ?></th>
    </tr>
    <tr class="form-field">
        <td class="w50">
            <table>
                <tbody>
                <tr class="avatar_field">
                    <td>
                        <img id="local_avatar_canvas" src="<?php echo ($bttf_match_local_avatar != '' ? wp_get_attachment_image_src($bttf_match_local_avatar,'thumbnail')[0] : $this->unknown);?>" class="avatar">
                        <input type="hidden" name="local_avatar" id="local_avatar" value="<?php if (isset
                        ($bttf_match_local_avatar)) echo $bttf_match_local_avatar; ?>" />
                    </td>
                    <td>
                        <input type="button" id="upload-image-button-1" class="button" value="<?php _e( 'Choose
                        image/avatar', 'bttf-almanac' ); ?>" />
                    </td>
                </tr>
                <tr class="info_field">
                    <td></td>
                    <td><p><i><?php _e('Upload or select your image and click on "Insert into Post" button.'); ?></i></p>
                    </td>
                </tr>
                <tr class="name_field">
                    <td>
                        <strong><label for="local_team"><?php _e( 'Name', 'bttf-almanac' ); ?></label></strong>
                    </td>
                    <td>
                        <input name="local_team" type="text" id="local_team" value="<?php if (isset($bttf_match_local_team)) echo $bttf_match_local_team; ?>" class="regular-text">
                    </td>
                </tr>
                <tr class="result_field">
                    <td>
                        <strong><label for="local_score"><?php _e( 'Score', 'bttf-almanac' ); ?></label></strong>
                    </td>
                    <td>
                        <input name="local_score" type="text" id="local_score" value="<?php if (isset($bttf_match_local_score)) echo $bttf_match_local_score; ?>" class="regular-text">
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
        <td class="w50">
            <table>
                <tbody>

                <tr class="avatar_field">
                    <td>
                        <img id="visitor_avatar_canvas" src="<?php echo ($bttf_match_visitor_avatar != '' ? wp_get_attachment_image_src($bttf_match_visitor_avatar,'thumbnail')[0] : $this->unknown);?>" class="avatar">
                        <input type="hidden" name="visitor_avatar" id="visitor_avatar" value="<?php if (isset
                        ($bttf_match_visitor_avatar)) echo $bttf_match_visitor_avatar; ?>" />
                    </td>
                    <td>
                        <input type="button" id="upload-image-button-2" class="button" value="<?php _e( 'Choose
                        image/avatar', 'bttf-almanac' ); ?>" />
                    </td>
                </tr>
                <tr class="info_field">
                    <td></td>
                    <td><p><i><?php _e('Upload or select your image and click on "Insert into Post" button.'); ?></i></p>
                    </td>
                </tr>
                <tr class="name_field">
                    <td>
                        <strong><label for="visitor_team"><?php _e( 'Name', 'bttf-almanac' ); ?></label></strong>
                    </td>
                    <td>
                        <input name="visitor_team" type="text" id="visitor_team" value="<?php if (isset($bttf_match_visitor_team)) echo $bttf_match_visitor_team; ?>" class="regular-text">
                    </td>
                </tr>
                <tr class="result_field">
                    <td>
                        <strong><label for="visitor_score"><?php _e( 'Score', 'bttf-almanac' ); ?></label></strong>
                    </td>
                    <td>
                        <input name="visitor_score" type="text" id="visitor_score" value="<?php if (isset($bttf_match_visitor_score)) echo $bttf_match_visitor_score; ?>" class="regular-text">
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
</table>