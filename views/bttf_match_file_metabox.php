<p><i><?php _e('Here you may upload a single PDF file containing the Back to the Future Almanac Page that shows
this result.', 'bttf-almanac'); ?></i></p>
<p><i><?php _e('IMPORTANT: Once uploaded the desired file, click on "File URL" button first and then on "Insert into
post".', 'bttf-almanac'); ?></i></p>
<table class="fullwidth">
    <tr class="form-field form-required">
        <th scope="row"><label for="im_manual_file"><?php _e( 'Select file', 'bttf-almanac' ); ?></label></th>
        <td>
            <input class="fullwidth" type="text" name="match_file" id="match_file" value="<?php if (isset($bttf_match_file)) echo $bttf_match_file; ?>" readonly="readonly"/>
        </td>
        <td class="button-cell">
            <input type="button" id="upload-match-file-button" class="button" value="<?php $bttf_match_file == '' ? _e( 'Upload file', 'bttf-almanac' ) : _e( 'Change file', 'bttf-almanac' ) ?>" />
            <input type="button" id="delete-match-file-button" class="button" value="<?php _e( 'Delete file', 'bttf-almanac' )?>" style="display: none" />
        </td>
    </tr>
</table>
